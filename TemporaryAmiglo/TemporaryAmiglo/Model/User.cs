﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace Amiglo
{
    public class User
    {
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email
        {
            get { return Email; }
            set
            {
                Email = value ?? throw new ArgumentException("Email must not be null");
            }
        }
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username
        {
            get { return Username; }
            set
            {

                Username = value ?? throw new ArgumentException("Username must not be null"); ;
            }
        }
        /// <summary>
        /// Gets or sets the university.
        /// </summary>
        /// <value>
        /// The university.
        /// </value>
        public string University
        {
            get { return University; }
            set
            {
                University = value ?? throw new ArgumentException("University must not be null");
            }
        }
        /// <summary>
        /// Gets or sets the interests.
        /// </summary>
        /// <value>
        /// The interests.
        /// </value>
        public List<String> interests { get; private set; }
        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        public string Location
        {
            get { return Location; }
            set
            {
                Location = value ?? throw new ArgumentException("Location must not be null");
            }
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        private string password;
        /// <summary>
        /// Gets or sets the origin.
        /// </summary>
        /// <value>
        /// The origin.
        /// </value>
        public string Origin
        {
            get { return Origin; }
            set
            {
                Origin = value ?? throw new ArgumentException("Origin must not be null");
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is registered.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is registered; otherwise, <c>false</c>.
        /// </value>
        public bool isRegistered { get; private set; }


        private bool checkRegistrationStatus()
        {
            foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                string value = (string)pi.GetValue(this);
                if (String.IsNullOrEmpty(value))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Encrypts the password.
        /// </summary>
        /// <param name="password">The password.</param>
        private void encryptPassword(string password)
        {
            //use sha-1 encryption
            this.password = password;
        }

        /// <summary>
        /// Stores the interests.
        /// </summary>
        /// <param name="interests">The interests.</param>
        private void storeInterests(string interests)
        {
            char[] seperator = { ',' };

            string[] input = interests.Split(seperator);

            foreach (var interest in input)
            {
                this.interests.Add(interest);
            }
        }



    }
}
