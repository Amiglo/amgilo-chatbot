﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TemporaryAmiglo
{
    public class Conversation
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string LastMessage { get; set; }
    }
}
