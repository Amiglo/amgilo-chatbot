﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TemporaryAmiglo
{
    public class Message
    {
        public string Id { get; set; }
        public string TopicId { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public string ConversationId { get; internal set; }
    }
}

