﻿using Amiglo;
using System;
using System.Collections.Generic;
using System.Text;

namespace TemporaryAmiglo.Interfaces
{
    public interface ISettings
    {
        User User { get; set; }
        void Save();
    }
}
