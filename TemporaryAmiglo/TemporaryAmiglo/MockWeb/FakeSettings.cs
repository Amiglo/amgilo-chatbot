﻿using Amiglo;
using System;
using System.Collections.Generic;
using System.Text;
using TemporaryAmiglo.Interfaces;

namespace TemporaryAmiglo.MockWeb
{
    public class FakeSettings : ISettings
    {
        public User User { get; set; }
        public void Save() { }
    }
}
