﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amiglo;

namespace TemporaryAmiglo.Droid
{
    class FakeWebService
    {
        public int SleepDuration { get; set; }

        public FakeWebService()
        {
            SleepDuration = 1;
        }

        private Task Sleep()
        {
            return Task.Delay(SleepDuration);
        }

        public async Task<User> Login(
            string username, string password)
        {
            await Sleep();
            return new User {Username = username};
        }

        public async Task<User> Register(User user)
        {
            await Sleep();
            return user;
        }

        public async Task<Conversation[]> GetConversations(string userId)
        {
            await Sleep();
            return new[] {
                new Conversation {
                    Id = "1",
                    UserId = "2",
                    Username =
                        "bobama",
                    LastMessage = "Hey!"
                },
                new Conversation {
                    Id = "1",
                    UserId = "3",
                    Username =
                        "bobloblaw",
                    LastMessage = "Have you seen that new movie?"
                },
                new Conversation {
                    Id = "1",
                    UserId = "4",
                    Username =
                        "gmichael",
                    LastMessage = "What?"
                },
            };
        }
        public async Task<Message[]> GetMessages(string conversationId)
        {
            await Sleep();
            return new[]
            {
                new Message
                {
                    Id = "1",
                    ConversationId = conversationId,
                    UserId = "2",
                    Text = "Hey",
                    Date = DateTime.Now.AddMinutes(-15),
                },
                new Message
                {
                    Id = "2",
                    ConversationId = conversationId,
                    UserId = "1",
                    Text = "What's Up?",
                    Date = DateTime.Now.AddMinutes(-10),
                },
                new Message
                {
                    Id = "3",
                    ConversationId = conversationId,
                    UserId = "2",
                    Text = "Have you seen that new movie?",
                    Date = DateTime.Now.AddMinutes(-5),
                },
                new Message
                {
                    Id = "4",
                    ConversationId = conversationId,
                    UserId = "1",
                    Text = "It's great!",
                    Date = DateTime.Now,
                },
            };
        }
        public async Task<Message> SendMessage(Message message)
        {
            await Sleep();
            return message;
        }


    }
}