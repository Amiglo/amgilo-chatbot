﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TemporaryAmiglo.Droid
{
    public class MessageViewModel : BaseViewModel
    {
        public Conversation[] Conversations { get; private set; }
        public Conversation Conversation { get; set; }
        public Message[] Messages { get; private set; }
        public string Text { get; set; }
    }
}